--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
-----------------------------------------------------------------
-- data.lua
-- Permite almacenar variables disponibles para distintas escenas 
--(En lugar de usar variables globales)
-----------------------------------------------------------------

local Datos = {}
Datos.tema = {}
Datos.introduccion = {}
Datos.actividad = {}
Datos.pregunta = {}
Datos.scenes = {"sceneTemas","sceneActividades","sceneIntroduccion","sceneEjercicios","sceneGameover","sceneCompleta","sceneCreditos"}
return Datos
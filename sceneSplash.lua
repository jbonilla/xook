--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
------------------------------------------------------------------------
-- sceneSplash.lua
-- Pantalla de inicio, muestra el logotipo
------------------------------------------------------------------------
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- Invoke file with "global variables"
local Datos = require( "data" )


local function startEvent( event )
	storyboard.gotoScene("sceneCero","slideLeft",1200)
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	local fondo = display.newImageRect( "assets/fondo.png", display.contentWidth,display.contentHeight) 
	fondo.x = display.contentCenterX
	fondo.y = display.contentCenterY
	fondo:addEventListener( "tap", startEvent )
	group:insert( fondo )
	
	local splash = display.newImageRect( "assets/splash.png", 780,780) 	
	splash.x = display.contentCenterX
	splash.y = display.contentCenterY - 50
	splash:addEventListener( "tap", startEvent )
	group:insert( splash )
	
	-- Intro sound (XOOK)
	if(Datos.sound)then
		audio.play( Datos.gameStart, {channel = 1} )
	end
	
	timer.performWithDelay( 900, startEvent )
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene
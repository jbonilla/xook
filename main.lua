--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
-------------------------------------------------------------
-- main.lua
-- Función principal; inicia aplicación, carga base de datos
-------------------------------------------------------------

-- Require widget and storyboard libraries 
local widget = require( "widget" )
local storyboard = require( "storyboard" )

-- Invoke file with "global variables"
local Datos = require( "data" )

-- Require sqlite3
require "sqlite3"

-- Set the directory for the DB and create it
local path = system.pathForFile("BD2.sqlite", system.ResourceDirectory)
db = sqlite3.open(path)

-- Module for loading & saving data
local loadsave = require( "loadsave" )

-- Invoke file with "saved data"
local dP = loadsave.loadTable("permanentdata.json") --datosPermanentes

---If the file permanentdata is empty create the basic tables
if (dP == nil) then 
	dP = {}
	dP.tema = {}
	dP.actividad = {}
end

-- Hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- Set the background to white
display.setDefault( "background", 255, 255, 255 )

--Variable for overlayOpciones
Datos.showing = false

--Load sounds
Datos.sound = true
Datos.gameComplete = audio.loadSound ("assets/sounds/steel.mp3")
Datos.gameOver = audio.loadSound ("assets/sounds/wooden.mp3")
Datos.gameStart = audio.loadSound("assets/sounds/xook.mp3")

--Load "THEME" table---------
local sql = "SELECT * FROM THEME"
local increment = 1

--Extract data from DB - temas
for row in db:nrows(sql)do
	
	Datos.tema[increment] = {}
	Datos.tema[increment].id = row.idTheme
	Datos.tema[increment].nombre = row.name
	Datos.tema[increment].imagen = row.image
	Datos.tema[increment].nivel = row.level
		

	if (dP.tema[increment] ~= nil) then
		Datos.tema[increment].activo = dP.tema[increment].activo
		Datos.tema[increment].completado = dP.tema[increment].completado
	else
		dP.tema[increment] = {}
		dP.tema[increment].activo = row.active
		dP.tema[increment].completado = row.complete
		Datos.tema[increment].activo = row.active
		Datos.tema[increment].completado = row.complete
	end
		
	increment = increment + 1
end


--Load "ACTIVITY" table--------- 
sql = "SELECT * FROM ACTIVITY" 
increment = 1


--Extract data from DB - Actividades
for row in db:nrows(sql)do

	Datos.actividad[increment] = {}
	Datos.actividad[increment].id = row.idActivity
	Datos.actividad[increment].idTheme = row.THEME_idTheme
	Datos.actividad[increment].nombre = row.name
	Datos.actividad[increment].tipo = row.type
	
	if (dP.actividad[increment] == nil)then --Theme does exist but activity doesn't
		dP.actividad[increment] = {}
		dP.actividad[increment].completado = row.completado
		Datos.actividad[increment].completado = row.completado
	else 
		Datos.actividad[increment].completado = dP.actividad[increment].completado
	end
	increment = increment + 1
end
loadsave.saveTable(dP, "permanentdata.json")

--Load "INTRODUCTION" table--------- 
sql = "SELECT * FROM INTRODUCTION"
increment = 1

--Extract data from DB - Introduccion
for row in db:nrows(sql)do
	
	Datos.introduccion[increment] = {}
	Datos.introduccion[increment].id = row.idIntroduction
	Datos.introduccion[increment].idActividad = row.ACTIVITY_idActivity
	Datos.introduccion[increment].texto = row.directory
	Datos.introduccion[increment].alto = row.height
	
	increment = increment + 1
end


--Load "QUESTION" table---------
sql = "SELECT * FROM QUESTION"
local idQuestion

--Extract data from DB - pregunta
for row in db:nrows(sql)do
	idQuestion = row.idQuestion

	Datos.pregunta[idQuestion] = {}
	Datos.pregunta[idQuestion].idActivity = row.ACTIVITY_idActivity
	Datos.pregunta[idQuestion].Texto = row.directory 
	Datos.pregunta[idQuestion].Tipo = row.type
	Datos.pregunta[idQuestion].Ancho = row.width
	Datos.pregunta[idQuestion].Alto = row.height

end

--Load "ANSWER" table---------
sql = "SELECT * FROM ANSWER"
local idAnswer

--Extract data from DB - respuesta
for row in db:nrows(sql)do	
	idQuestion = row.QUESTION_idQuestion
	
	if (Datos.pregunta[idQuestion] == nil) then
		error("Answer: "..row.idAnswer.."is linked to the non existent question: "..idQuestion)
	else
		idAnswer = #Datos.pregunta[idQuestion] + 1
		Datos.pregunta[idQuestion][idAnswer] = {}
		Datos.pregunta[idQuestion][idAnswer].id = row.idAnswer
		Datos.pregunta[idQuestion][idAnswer].Opcion = row.directory
		Datos.pregunta[idQuestion][idAnswer].Correcta = row.right
		Datos.pregunta[idQuestion][idAnswer].Ancho = row.width
		Datos.pregunta[idQuestion][idAnswer].Alto = row.height	

	end

end


if db and db:isopen() then
	db:close()
end	
sql = nil
idQuestion = nil
idAnswer = nil
increment = nil

----- load sceneSplash.lua -----
storyboard.gotoScene("sceneSplash","slideLeft",1)



local function handleButtonAlert( event )
	if "clicked" == event.action then
        local i = event.index
        if i == 1 then --If yes
			storyboard.gotoScene(Datos.scenes[2],"slideRight",200)
        end
    end
end

local function onKeyEvent( event )
   local phase = event.phase
   local keyName = event.keyName
   local sceneName = storyboard.getCurrentSceneName()

	if ( "back" == keyName and phase == "up" ) then
		if(sceneName==Datos.scenes[1])then
			native.requestExit()
		elseif(storyboard.isOverlay) then
			storyboard.hideOverlay()
		elseif(sceneName==Datos.scenes[2] or sceneName == Datos.scenes[7]) then
			storyboard.gotoScene(Datos.scenes[1],"slideRight",200)
		elseif(sceneName==Datos.scenes[4]) then
			native.showAlert("Salir","El progreso se perderá, aún así desea salir", {"Si", "No"},handleButtonAlert)
		else
			storyboard.gotoScene(Datos.scenes[2],"slideRight",200)
		end
		return true
	end
end

--add the key callback
Runtime:addEventListener( "key", onKeyEvent )

--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
----------------------------------------------------------------
-- sceneCreditos.lua
-- Ventana de Creditos
-----------------------------------------------------------------
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require( "widget" )


function scene:createScene( event )
	local group = self.view
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	--Hide Settings Button
	local display_stage = display.getCurrentStage()
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
		end
		i = i + 1
	end
	
	local message = [[-------------CREDITOS-------------
Creado por: Julio Bonilla 
	
Contenidos:
Castañeda De I. P., Érik
Geometría analítica en el espacio
México
Facultad de Ingeniería-UNAM, 2003

Ejercicios. Adaptación del Laboratorio 
Virtual de Matemáticas. Agradecimiento
a los participantes en el desarrollo 
de los ejercicios modelo:
~ Casiano Aguilar Morales
~ Hortencia Caballero López
~ Juan Velázquez Torres
~ Luis Humberto Soriano Sánchez
~ María del Rocío Ávila Núñez
~ María Sara Valentina Sánchez Salinas
~ Mayverena Jurado Pineda
~ Ricardo Martínez Gómez
~ Rosalba Rodríguez Chávez
~ Sergio Arzamendi Pérez
~ Sergio Carlos Crail Corzas
	
Diseño gráfico: Marina Casañas

Sonidos:
~ 103628_benboncan_large-anvil-steel-hammer by Benboncan https://freesound.org/people/Benboncan/sounds/103628/
~ 161592_jorickhoofd_wooden-log-falling by jorickhoofd http://www.freesound.org/people/jorickhoofd/sounds/161592/ 
]]
	
	local options = 
	{
		text = message,
		width = display.contentWidth * .80,     
		height = 0,   
		font = "Montserrat-Regular",   
		fontSize = 35,
		align = "left"  
	}
	local textBox = display.newText( options )
	textBox:setFillColor( 0, 0, 0)
	textBox.x = display.contentCenterX
	textBox.y = display.contentCenterY + 300
	
	local box = display.newImageRect( "assets/burbujab.png", display.contentWidth * .90, textBox.height+20)
	box.x = textBox.x 
	box.y = textBox.y 
	
	local scrollView = widget.newScrollView
	{
		left = 0,
		top = 200,
		width = display.viewableContentWidth,
		height = display.viewableContentHeight,
		scrollWidth = box.contentWidth,
		scrollHeight = box.height,
		topPadding = 00,
		bottomPadding = 350,
		hideBackground = true,
		horizontalScrollingDisabled = true,
		verticalScrollingDisabled = false,
		isBounceEnabled = false,
	}

	scrollView:insert( box )
	scrollView:insert( textBox )
	group:insert( scrollView )
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

return scene
--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
---------------------------------------------------------------
-- sceneGameComplete.lua
-- All the questions were successfully answered
---------------------------------------------------------------
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require( "widget" )

-- Invoke file with "global variables"
local Datos = require( "data" )

-- Module for loading & saving data
local loadsave = require( "loadsave" )

-- Invoke file with "saved data"
local datosPermanentes = loadsave.loadTable("permanentdata.json")
local dP = datosPermanentes

--Handle of the timer
local htimer 

local function listener( event )
	if (htimer ~= nil and Datos.sound == true) then
		audio.play( Datos.gameComplete,{channel = 1} )
	end
end

--Handlers
local function handleButtonEvent( event )
	storyboard.gotoScene("sceneActividades","slideLeft",600)
end

local function handleTapEvent( event )
	transition.cancel( "transition" )
	audio.stop( )
	htimer = nil
	storyboard.gotoScene("sceneActividades","slideLeft",1200)
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	local tamenX = 408
	local tamenY = 544
	local animaX
	local animaO
	local animaO2
	local animaK
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	--Hide Settings Button
	local display_stage = display.getCurrentStage()
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
		elseif(display_stage[i].id == "salir") then
			display_stage[i].isVisible = false
		end
		i = i + 1
	end
	
	local function completeListener ( obj )
	----- End of animation; load sceneActividades.lua
		
		fondo = nil
		letterX = nil
		letterO = nil
		letterO2 = nil
		letterK = nil
		
		tamenX = nil
		tamenY = nil
		animaX = nil
		animaO = nil
		animaO2 = nil
		animaK = nil
		
		transition.cancel()
		storyboard.gotoScene("sceneActividades","slideLeft",1400)
	end
	
	
	----------------------Lets Update----------------------------------
	
	--Values from esceneActividades----
	local idActividades = Datos.actividadTarget 
	local idTemas = Datos.temaTarget
	
	-----This activity was completed
	dP.actividad[idActividades].completado =  1  
	Datos.actividad[idActividades].completado =  1
	
	local temaUpdate = 1
	
	for id=1, #Datos.actividad do
		if(Datos.actividad[id].tipo == 2) then --Only if it's an exercise
			if(Datos.actividad[id].completado ~= 1) then --All the activities from this theme were completed?
				temaUpdate = 0 --If not, we don't need to update 
				break --go out
			end
		end
	end
	
	if (temaUpdate == 1) then --If all the activities in the theme are complete
		dP.tema[idTemas].completado = 1
		Datos.tema[idTemas].completado = 1
		
		local nivel = Datos.tema[idTemas].nivel --Alias of the actual lvl
		local temaUpgrade = 1
		for tema=1, #Datos.tema do --Let´s check in (almost)all the themes
			if(Datos.tema[tema].nivel==nivel) then --Checking only if it´s the same lvl
				if(Datos.tema[idTemas].completado ~= 1) then --If one from the lvl isn´t complete
					temaUpgrade = 0 --We don´t upgrade
					break --Go out
				end
			elseif(Datos.tema[tema].nivel==nivel+1) then --We are one lvl up
				if(temaUpgrade==1)then --If the previous lvl was complete
					Datos.tema[idTemas].activo = 1
					dP.tema[idTemas].activo = 1
				end
			elseif(Datos.tema[tema].nivel>nivel+1) then --We are more than one lvl up
				break
			end
		end
	end
	
	loadsave.saveTable(dP, "permanentdata.json")  --Save in the file
	---------------------------- End of update -----------------------------------------------------
	
	
	-- Create the result text
	local optionsTexto = 
	{
		text = Datos.actividad[idActividades].nombre .. " Completado",     
		x = 100,
		y = 150,
		width = 550,
		height = 0,
		font = "Montserrat-Regular",   
		fontSize = 80,
		align = "center"  
	}
	local resultText = display.newText( optionsTexto )
	resultText.anchorX = 0
	resultText.anchorY = 0
	resultText:setFillColor(1, 1, 1)
	group:insert( resultText )
	
	local fondo = display.newImageRect( "assets/fondo.png", display.contentWidth,display.contentHeight) 
	fondo.x = display.contentCenterX
	fondo.y = display.contentCenterY
	fondo.id = 0
	fondo:addEventListener( "tap", handleTapEvent )
	group:insert( fondo )
	
	local letterX = display.newImageRect( "assets/X.png", tamenX,tamenY) 
	letterX.x = -350
	letterX.y = 420
	letterX.id = 1
	letterX:addEventListener( "tap", handleTapEvent )
	group:insert( letterX )
	
	local letterO = display.newImageRect( "assets/O.png", tamenX,tamenY) 
	letterO.x = 1200
	letterO.y = 310
	letterO.id = 2
	letterO:addEventListener( "tap", handleTapEvent )
	group:insert( letterO )
	
	local letterO2 = display.newImageRect( "assets/O.png", tamenX,tamenY) 
	letterO2.x = -350
	letterO2.y = 1200
	letterO2.id = 3
	letterO2:addEventListener( "tap", handleTapEvent )
	group:insert( letterO2 )
	
	local letterK = display.newImageRect( "assets/K.png", tamenX,tamenY)
	letterK.x = 1200
	letterK.y = 1090
	letterK.id = 4
	letterK:addEventListener( "tap", handleTapEvent )
	group:insert( letterK )
	
	-----------------Start animating--------------------
	
	htimer = timer.performWithDelay( 950, listener )
	animaX = transition.to(letterX,{
	time = 1000,
	x = 230,
	y = 620,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 1900, listener ) })
	
	animaO = transition.to(letterO,{
	time = 1000,
	delay = 1000,
	x = 520,
	y = 510,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 2850, listener )})	
		
	animaO2 = transition.to(letterO2,{
	time = 1000,
	delay = 2000,
	x = 310,
	y = 1000,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 3800, listener )})
	
	animaK = transition.to(letterK,{
	time = 1000,
	delay = 3000,
	x = 600,
	y = 890,
	transition = easing.linear,
	tag="transition",
	onComplete = completeListener})
		
end



-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene
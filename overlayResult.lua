--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
---------------------------------------------------------------------
-- overlayResult.lua
-- Muestra la imagen de falso o correcto
---------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()


function scene:createScene( event )
	local group = self.view
	local resultado = event.params.result
	
	if (resultado == 0) then
		dir = "assets/falso.png"
	else
		dir = "assets/correcto.png"
	end
	
	local imgResultado = display.newImageRect(dir, display.contentWidth,display.contentHeight)
	imgResultado.x = display.contentCenterX 
	imgResultado.y = display.contentCenterY
	group:insert(imgResultado)
end



-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

return scene
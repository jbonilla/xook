--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
--------------------------------------------------------------------
-- overlayOpciones.lua
-- Despliega opciones (sonidos y créditos).
--------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require( "widget" )

-- Invoke file with "global variables"
local Datos = require( "data" )

-- Enable/disable sound
musik = function  ( event )
	Datos.sound = not Datos.sound
	Datos.showing = not Datos.showing
	storyboard.hideOverlay()
end


handleTapEventCredit = function  ( event )
	Datos.showing = not Datos.showing
	storyboard.gotoScene("sceneCreditos","zoomOutInFade",200)
end


-- Called when the scene's view does not exist:
function scene:createScene( event )

	local group = self.view
	
	local display_stage = display.getCurrentStage()
	
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "subTitleBarBottom") then
			break
		end
		i = i + 1
	end

	local options =
	{
		width = 82.5,
		height = 82.5,
		numFrames = 4,
		sheetContentWidth = 165,  
		sheetContentHeight = 165  
	}
	local imageSheet = graphics.newImageSheet( "assets/adicion.png", options )


	--Custom button for musik
	local musica 
	if(Datos.sound) then
		musica = widget.newButton
		{
		   id = "musica",
		   sheet = imageSheet,  
		   defaultFrame = 1,     
		   overFrame = 3,        
		   onRelease  = musik
		}
	else
		musica = widget.newButton
		{
		   id = "musica",
		   sheet = imageSheet,  
		   defaultFrame = 3,     
		   overFrame = 1,        
		   onRelease  = musik
		}
	end
	musica.x = 642.5
	musica.y = display_stage[i].y + display_stage[i].height/2 + musica.height/2
	
	local creditos = display.newImageRect( imageSheet, 2, 82.5, 82.5 )
	creditos.x = 730
	creditos.y = display_stage[i].y + display_stage[i].height/2 + creditos.height/2
	creditos:addEventListener( "tap", handleTapEventCredit )
	
	group:insert( musica )
	group:insert( creditos )
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

return scene
--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
---------------------------------------------------------------------
-- sceneEjercicios.lua
-- Selección de preguntas y respuestas al azar, despliegue de éstas, 
--sistema de vidas.
---------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require( "widget" )

--Modify time to "hear" the taps
system.setTapDelay( 5 )

-- Invoke file with "global variables"
local Datos = require( "data" )


--Interface (constant objects)
local scrollView
local boton
local gauge
--Interface (variables for the questions)
local questionBox
local question 
local answerGroup
local answerBox = {}
local answer = {}
local radioButton = {}
local buttonBox = {}
--Variables
local vidas = 1
local ciclo = 1
local cantidadPreguntas = 5 --How many questions will be showed 
local secuencia = {} --Array that contains the questions to be shown 
local numeroRespuestas
local correcta
local choose
local resultado
--Variables inherited from sceneActividades
local idActividades = Datos.actividadTarget   
local idTemas = Datos.temaTarget

-----functions
local continua
local incorrecta

--Handlers
local function onSwitchPress( event )

	if (boton:getLabel()=="Continuar") then -- Used to disable the radio button
		for k=1, #buttonBox do
			buttonBox[k]:setState({isOn=false})
		end
		buttonBox[choose]:setState({isOn=true})
		return true
	end
	
	
	if (event.phase=="began") then
		for k=1, #buttonBox do
			buttonBox[k]:setState({isOn=false})
		end
		choose = event.target.id

		correcta = Datos.pregunta[secuencia[ciclo]][choose].Correcta
		boton:setEnabled(true)
		boton:setFillColor(1,1)

		buttonBox[choose]:setState({isOn=true})
		return true
	end
end

local function handleButtonEvent( event )

	if (boton:getLabel()=="Continuar") then
		continua()
	else --First time that it's pressed 
		answerBox[1]:setFillColor(0,1,0)
		if(correcta~= 1)then --If the answer was wrong	
			answerBox[choose]:setFillColor(1,0,0)
			local options =
			{
				effect = "flipFadeOutIn",
				time = 400,
				--isModal = true,
				params = {
				result = 0}
			}
			storyboard.showOverlay( "overlayResult", options)
			vidas = vidas + 1 --Loose one life
			gaugeSprite:setSequence( "health" .. vidas )
			gaugeSprite:play()
			incorrecta()
		else
			local options =
			{
				effect = "flipFadeOutIn",
				time = 400,
				--isModal = true,
				params = {
				result = 1}
			}
			storyboard.showOverlay( "overlayResult", options)
		end
		
		--Tal vez tenga que ir arriba (en cada caso)
		if (ciclo < cantidadPreguntas) then -- # of questions to be shown
			storyboard.hideOverlay( true, "fade", 2000 )
		else
			storyboard.hideOverlay( "overlayResult", 2000)
		end
		
		boton:setLabel("Continuar")
	end
end

local function after( event )
	resultado:removeSelf()
	resultado = nil
end

local function handleTapEvent( event )
	if (event.numTaps > 1 ) then
		local target = event.target.id
		
		if(target=="question") then
			local options =
			{
				effect = "zoomOutInFade",
				time = 400,
				isModal = true,
				params = {
				filename = question.filename,
				Alto = question.height,
				Ancho = question.width,
				Ejercicios = true}
			}
			storyboard.showOverlay( "overlayZoom", options)
		else
			local options =
			{
				effect = "zoomOutInFade",
				time = 400,
				isModal = true,
				params = {
					filename = answer[target].filename,
					Alto = answer[target].height,
					Ancho = answer[target].width,
					Ejercicios = true}
			}
			storyboard.showOverlay( "overlayZoom", options)
		end
	end
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	--Hide settings button, change titleBarText
	local display_stage = display.getCurrentStage()
	local i = 2
	local j
	while display_stage[i] ~= nil do
		
		if(display_stage[i].id == "titleBarText") then
			display_stage[i].isVisible = false
			j = i
		elseif(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
			--break
		end
		i = i + 1
	end
	
	-- Load theme title bar text (replace XOOK)
	local dir = Datos.tema[idTemas].imagen  --nombre de las imagenes BD
	local titleBarText2 = display.newImageRect(dir,380,110) 
	titleBarText2.x = display.contentCenterX
	titleBarText2.y = display_stage[j].y
	
	-- Create a ScrollView
	scrollView = widget.newScrollView
	{
		left = 0,
		top = 200,
		width = display.ContentWidth,
		height = display.ContentHeight,
		scrollWidth = 1800,
		scrollHeight = 1400,
		rightPadding = 200,
		topPadding = 0,
		bottomPadding = 200,
		hideBackground = true,
		horizontalScrollingDisabled = false,
		verticalScrollingDisabled = true,
		isBounceEnabled = false
	}
	
	local buttonOptions = {
	   width = 220,
	   height = 120,
	   numFrames = 2,
	}
	
	local buttonSheet = graphics.newImageSheet( "assets/navButtonG.png", buttonOptions )
	
	
	boton = widget.newButton
	{
		id=i,
		label = "Calificar",
		labelAlign = "center",
		sheet = buttonSheet,  --reference to the image sheet
		defaultFrame = 2,     --number of the "default" frame
		overFrame = 1,        --number of the "over" frame
		font = "Montserrat-Regular",
		fontSize = 30,
		labelColor = { default = {0,0,0}, over = {0,0,255} },
		isEnabled = false,
		onRelease = handleButtonEvent
	}
	boton.xScale = 2
	
	local options = {
		width = 150,
		height = 50,
		numFrames = 4,
	}
	local gaugeSheet = graphics.newImageSheet("assets/sheetLife.png",options)
	
	local sequenceData = {
	   { name = "health1", start=1, count=1, time=0, loopCount=1 },
	   { name = "health2", start=2, count=1, time=0, loopCount=1 },
	   { name = "health3", start=3, count=1, time=0, loopCount=1 },
	   { name = "health4", start=4, count=1, time=0, loopCount=1 }
	}

	gaugeSprite = display.newSprite( gaugeSheet, sequenceData )
	gaugeSprite.x = 660
	gaugeSprite.y = display_stage[4].y
	
	-----Which questions are going to be loaded------
	local totalPreguntas = 0
	local temporalTable = {}    -- new temporal array
	j = 1
	
	for key,value in pairs(Datos.pregunta) do
		if (value.idActivity == idActividades) then
			totalPreguntas = totalPreguntas + 1
			temporalTable[j] = key
			j = j + 1
		end
	end
	
	--Create and aleatory array that contains the index of the questions to be shown
	for i = 1, cantidadPreguntas do
		local pos = math.random( totalPreguntas - (i-1))
		local numero =  table.remove(temporalTable,pos)
		secuencia[i] = numero
	end	
	--------questions to be loaded-----------
	
	--Clear temporal variables
	totalPreguntas = nil
	temporalTable = nil
	j = nil
	
	group:toFront() 
	scrollView:insert( boton )
	group:insert( gaugeSprite )
	group:insert( scrollView )
	group:insert( titleBarText2 )
	
end


-- Called immediately before scene is moved on screen:
function scene:willEnterScene( event )
	local group = self.view


	--button disabled until the user makes a choice
	boton:setEnabled(false)
	boton:setFillColor(.5,.5)
	boton:setLabel("Calificar")
	
	local numeroPregunta = secuencia[ciclo] --Id of the question to be shown

	----Display question-----
	local dir = Datos.pregunta[numeroPregunta].Texto 
	local alto = Datos.pregunta[numeroPregunta].Alto
	local ancho = 600
	
	question = display.newImageRect(dir,ancho,alto)
	
	question.x = ancho/2 + 100
	question.y = question.height/2 
	question.id = "question"
	question.filename = dir

	question:addEventListener( "tap", handleTapEvent ) --Double tap to zoom
	
	---------Answers-------
	answerGroup = display.newGroup() --Group for the Answers
	local answersheight = question.height/2 + question.y --Variable with the total height of the Questions & Answers
	local answersWidth = ancho
	
	------Random-------------------------------------
	local totalRespuestas = #Datos.pregunta[numeroPregunta]
	local temporalTable = {}    -- new array
	for i=1, totalRespuestas do -- Array initialized with consecutive numbers 
	  temporalTable[i] = i
	end

	--Create an aleatory array with de id of the answer to be showed
	local numeroRespuesta = {}
	for i=1, totalRespuestas do
		local pos = math.random( totalRespuestas - (i-1) )
		local numero =  table.remove(temporalTable,pos)
		numeroRespuesta[i] = numero
	end	
	------ End of random------------------------------
	
	--Draw the answers-------
	for i=1, totalRespuestas do
		local nR = numeroRespuesta[i] --Alias
		
		if (Datos.pregunta[numeroPregunta].Tipo == 1) then
			
			local dir = Datos.pregunta[numeroPregunta][nR].Opcion --image dir
			local alto = Datos.pregunta[numeroPregunta][nR].Alto
			local ancho = Datos.pregunta[numeroPregunta][nR].Ancho
			if (ancho > 600) then
				ancho = 600
			end

			answer[nR] = display.newImageRect(dir,ancho,alto) 
			
			answersheight = answersheight + answer[nR].height/2 + 100
			answer[nR].x = answer[nR].width/2 + 120 
			answer[nR].y = answersheight 
			answer[nR].id = nR
			answer[nR].filename = dir

			answer[nR]:addEventListener( "tap", handleTapEvent )
			
			-- Box (rectangle) that will contain the answer
			answerBox[nR] = display.newImageRect( "assets/burbujab.png", answer[nR].width+50, answer[nR].height+10 ) 
			answerBox[nR].x = answer[nR].x - 10
			answerBox[nR].y = answersheight 
			answerBox[nR].id = nR
			
			if(answersWidth < answerBox[nR].width) then --If the box is smaller make it bigger than the answer
					answersWidth = answerBox[nR].width + 50
			end
			
			radioButton[nR] = widget.newSwitch
			{
				left = answerBox[nR].x-answerBox[nR].width/2,
				top = answer[nR].y-20,
				style = "radio",
				id = nR,
				initialSwitchState = false,
				onPress = onSwitchPress
			}
			buttonBox[nR] = radioButton[nR]
			
			answerBox[nR]:addEventListener( "touch", onSwitchPress ) --All the elements of the answer should activate the rb
			
			answerGroup:insert( answerBox[nR] )
			answerGroup:insert( answer[nR] )
			answerGroup:insert( radioButton[nR] )			
		else
			local dir = Datos.pregunta[numeroPregunta][nR].Opcion
			local alto = Datos.pregunta[numeroPregunta][nR].Alto
			if (alto > 330) then
				alto = 330
			end
			
			local ancho = Datos.pregunta[numeroPregunta][nR].Ancho
			if (ancho > 250) then
				ancho = 250
			end
			
			answer[nR] = display.newImageRect(dir,ancho,alto)
			local X =  question.x - question.width/2 + answer[nR].width/2 --+ 50
			
			if(nR<3) then
				answer[nR].x = X 
			else
				answer[nR].x = X + ancho + 100
				local anchonR = answer[nR].x + answer[nR].width/2
				if(answersWidth < (anchonR - 100) ) then
					answersWidth = anchonR - 100
				end
			end
      
			if(nR==1 or nR==3) then
				answer[nR].y = question.y + question.height/2  + answer[nR].height/2 + 75
			else
				answersheight = answersheight + alto + 87.5 --Will increment 2 times
				answer[nR].y = question.y + question.height/2 + alto*1.5 + 175
			end

			answer[nR].id = nR
			
			answer[nR].filename = dir
			
			answer[nR]:addEventListener( "touch", onSwitchPress )
			answer[nR]:addEventListener( "tap", handleTapEvent )
			
			answerBox[nR] = display.newImageRect( "assets/burbuja.png", answer[nR].width+40, answer[nR].height+60 ) 
			answerBox[nR].id = nR
			answerBox[nR].x = answer[nR].x 
			answerBox[nR].y = answer[nR].y + 10
			
			radioButton[nR] = widget.newSwitch
			{
				left = answerBox[nR].x-10,
				top = answer[nR].y+ answer[nR].height/2 ,
				style = "radio",
				id = nR,
				initialSwitchState = false,
				onPress = onSwitchPress
			}
			buttonBox[nR] = radioButton[nR]
			
			answerBox[nR]:addEventListener( "touch", onSwitchPress )

			answerGroup:insert( answerBox[nR] )
			answerGroup:insert( answer[nR] )
			answerGroup:insert( radioButton[nR] )
		end
	end
 
	questionBox = display.newImageRect( "assets/burbujab.png", answersWidth + 100, answersheight + 100 )
	questionBox.x = answersWidth/2 + 100
	questionBox.y = (answersheight/2) + 50
	
	boton.x = questionBox.x 
	boton.y =  answersheight + 200  
	
	scrollView:insert( questionBox )		
	scrollView:insert( question )
	scrollView:insert( answerGroup )
end


-- Called when scene is about to move offscreen, when scene is reloaded:
function scene:exitScene( event )
	local group = self.view
	
	for numeroRespuestas=#answerBox,1,-1  do
		answerBox[numeroRespuestas]:removeEventListener( "touch", onSwitchPress)
		answerGroup:remove( answerBox[numeroRespuestas] )
		answerBox[numeroRespuestas] = nil
		answerGroup:remove( answer[numeroRespuestas] )
		answer[numeroRespuestas] = nil
		answerGroup:remove( radioButton[numeroRespuestas] )
		radioButton[numeroRespuestas] = nil
	end
	
	scrollView:remove( answerGroup )
	display.remove( answerGroup )
	answerGroup = nil
	scrollView:remove( question )
	display.remove( question )
	question = nil
	scrollView:remove( questionBox )
	display.remove( questionBox )
	questionBox = nil
	
	buttonBox = {}
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local display_stage = display.getCurrentStage()
	
	display_stage[3].isVisible = true

	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
		end
		i = i + 1
	end

end


-- the following event is dispatched to continue or end the test
continua = function ()
	if(vidas==4) then
		--storyboard.hideOverlay( "fade",100 )
		storyboard.gotoScene("sceneGameover","slideLeft",300)
	else
		if (ciclo < cantidadPreguntas) then -- # of questions to be shown
			--storyboard.hideOverlay( true, "fade",100 )
			storyboard.reloadScene()
		else
			--storyboard.hideOverlay( "fade",100 )
			storyboard.gotoScene("sceneGameComplete","slideLeft",300)
		end
		ciclo = ciclo + 1
	end
end

incorrecta = function ()
	resultado = display.newImageRect( "assets/piedra.png", 50,50)
	resultado:toFront()
	
	if(vidas==2) then
		resultado.x = 660 - 257/3
	elseif(vidas==3) then
		resultado.x = 660 
	else
		resultado.x = 660 + 257/3
	end
	resultado.y = resultado.height/2 + 14
	
	transition.to( resultado, { time=1100, y=1200, rotation=720, alpha=0.2, transition=easing.outInBack } )
	timer.performWithDelay( 1800,after)
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "willEnterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )



return scene
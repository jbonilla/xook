--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
------------------------------------------------
-- sceneTemas.lua
-- Muestra los temas disponibles
------------------------------------------------
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()


-- Invoke file with "global variables"
local Datos = require( "data" )

-- forward declarations
local F_dibujaBoton
local F_dibujaLinea
local F_Stage
--Variables
local anchoBoton = 330
local altoBoton = 210


--Handler
local function handleButtonEvent( event )
	local target = event.target
	Datos.temaTarget = event.target.id 
	storyboard.gotoScene("sceneActividades","slideLeft",200)
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	local x1,x2,y,bandera,nLinea,nBoton 
	local nivel = 1
	local botonxnivel={}
	botonxnivel[nivel]=0
	y = altoBoton + 130 -- altoBoton + Margen
	
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	local display_stage = display.getCurrentStage()
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "salir") then
			display_stage[i].isVisible = true
		end
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = true
		end
		i = i + 1
	end
		
	local groupBackground = display.newGroup()
	
	for i=1, #Datos.tema do
		
		if (Datos.tema[i].nivel == nivel) then
			botonxnivel[nivel] = botonxnivel[nivel]+1
			nBoton = F_dibujaBoton(Datos.tema[i].id,i,nivel,botonxnivel[nivel])
			groupBackground:insert(nBoton)
			
			if (nivel>1) then
				-----Draw left lines---------
				nLinea = F_dibujaLinea( x2,y*botonxnivel[nivel], x2+50,y*botonxnivel[nivel] )
				groupBackground:insert(nLinea)
				
				if(bandera) then
					-----Draw vertical lines-----
					if(botonxnivel[nivel-1]>botonxnivel[nivel]) then
						nLinea = F_dibujaLinea( x2,y, x2,y*botonxnivel[nivel-1] )
					else
						nLinea = F_dibujaLinea( x2,y, x2,y*botonxnivel[nivel] )
					end
					groupBackground:insert(nLinea)
					bandera=false
				end
			end
		else
			x1 = 200 +(anchoBoton+100)*(nivel-1) + (anchoBoton*.5) 
			x2 = x1+50
			
			----Draw right lines------
			for j=1, botonxnivel[nivel] do
				nLinea = F_dibujaLinea(x1,y*j, x2,y*j)
				groupBackground:insert(nLinea)
			end
			
			nivel = nivel+1
			botonxnivel[nivel]=1
			bandera=true
			-----Draw the first element and the first line-----
			nBoton = F_dibujaBoton(Datos.tema[i].id,i,nivel,botonxnivel[nivel])	
			nLinea = F_dibujaLinea( x2,y, x2+50,y )
			groupBackground:insert(nBoton)
			groupBackground:insert(nLinea)
			
			-----Draw a vertical line if there's only one element---
			if((i+1)>#Datos.tema) then
				nLinea = F_dibujaLinea( x2,y, x2, y*botonxnivel[nivel-1] )
				groupBackground:insert(nLinea)
			end
			
		end		
	end
	
	if(x2==nil) then
		x2 = display.viewableContentWidth
	end
	
	-- Create a ScrollView
	local scrollView = widget.newScrollView
	{
		left = 0,
		top = 0,
		width = display.viewableContentWidth,
		height = display.viewableContentHeight,
		scrollWidth = x2*2,
		scrollHeight = y*6,
		rightPadding = 200,
		topPadding = 0,
		bottomPadding = 50,
		hideBackground = true,
		horizontalScrollingDisabled = false,
		verticalScrollingDisabled = false,
		isBounceEnabled = false,
	}

	scrollView:insert( groupBackground )
	group:insert( scrollView )
end


F_dibujaBoton = function (idTema,i,nivel,botonxnivel)
	local boton
	
	local options = {
	   width = 330,
	   height = 165,
	   numFrames = 2,
	}
	local buttonSheet = graphics.newImageSheet( "assets/blackboardSheet.png", options )
	
	boton = widget.newButton
		{
			id = idTema,
			label = Datos.tema[i].nombre,
			labelAlign = "center",
			sheet = buttonSheet,  
			defaultFrame = 1,     
			overFrame = 2,        
			fontSize = 40,
			font = "Montserrat-Regular",
			labelColor = { default = {1,1,1}, over = {1,1,1} },
			isEnabled = true,
			onRelease = handleButtonEvent
		}
		
	if(Datos.tema[i].activo == 0) then
		boton:setEnabled( false )
	end
	boton.x = 200+(anchoBoton+100)*(nivel-1)
	boton.y = (altoBoton+130)*botonxnivel 

	return boton
end

F_dibujaLinea = function (x1,y1,x2,y2)
	local linea = display.newLine(x1,y1,x2,y2)
	linea:setStrokeColor( 1, 1, 1)
	linea.strokeWidth = 10
	
	return linea
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene
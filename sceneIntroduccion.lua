--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
----------------------------------------------------------------
-- sceneIntroduccion.lua
-- Carga el módulo de introducción correspondiente
----------------------------------------------------------------

local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- Invoke file with "global variables"
local Datos = require( "data" )

--Forward declaration
local idTemas = Datos.temaTarget
local idActividad = Datos.actividadTarget

--Variable
local introImage = {}
	
-- handler: On double tap Zoom
local function handleTapEvent( event )
	if (event.numTaps > 1 ) then
		local target = event.target.id
		
		local options =
		{
			effect = "zoomOutInFade",
			time = 400,
			isModal = true,
			params = {
			filename = introImage[target].filename,
			Alto = introImage[target].height,
			Ancho = introImage[target].width,
			Ejercicios = false}
		}
		storyboard.showOverlay( "overlayZoom", options)
		
	end
end


	
-- Called when the scene's view does not exist:
function scene:enterScene( event )
	local group = self.view
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	local display_stage = display.getCurrentStage()
	local i = 2
	local j
	local ancho=600
	
	--Change titleBarText
	while display_stage[i] ~= nil do	
		if(display_stage[i].id == "titleBarText") then
			display_stage[i].isVisible = false
			j = i
			break
		end
		i = i + 1
	end
	
	-- Load theme title bar text (replace XOOK)
	local dir = Datos.tema[idTemas].imagen  --nombre de las imagenes BD
	local titleBarText2 = display.newImageRect(dir,380,110) 
	titleBarText2.x = display.contentCenterX
	titleBarText2.y = display_stage[j].y
	
	-- Create a ScrollView
	local scrollView = widget.newScrollView
	{
		left = 0,
		top = 200,
		width = display.viewableContentWidth,
		height = display.viewableContentHeight,
		scrollWidth = 1800,
		scrollHeight = 1600,
		rightPadding = 150,
		topPadding = 0,
		bottomPadding = 200,
		hideBackground = true,
		horizontalScrollingDisabled = false,
		verticalScrollingDisabled = false,
		isBounceEnabled = false
	}

	-- Load and show image
	j = 1
	for idintroduccion = 1, #Datos.introduccion do		
		
		if(Datos.introduccion[idintroduccion].idActividad == idActividad ) then
			local dir = Datos.introduccion[idintroduccion].texto --link of the image
			local alto = Datos.introduccion[idintroduccion].alto
				
			introImage[j] = display.newImageRect(dir,ancho,alto)
			introImage[j].x = 400+650*(j-1)
			introImage[j].y = display.contentCenterY - 200 --scrollView.top
			introImage[j].id = j
			introImage[j].filename = dir

			introImage[j]:addEventListener( "tap", handleTapEvent ) --Double tap to zoom
			
			--local box = display.newImageRect( "assets/burbujab.png", introImage[j].width+20, introImage[j].height+60)
			local box = display.newImageRect( "assets/burbujab.png", introImage[j].width+20, 1100)
			box.x = introImage[j].x - 10
			box.y = box.height/2
			
			scrollView:insert( box )
			scrollView:insert( introImage[j] )
			
			j = j + 1
		end
		
	end
	
	group:insert(titleBarText2)
	group:insert( scrollView )
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "enterScene", scene )


return scene
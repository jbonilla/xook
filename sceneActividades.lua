--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
-------------------------------------------
-- sceneActvidades.lua
-- Despliega la pantalla de actividades
--------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require( "widget" )

-- Invoke file with "global variables"
local Datos = require( "data" )

--Variables inherited from sceneActividades
local idTemas = Datos.temaTarget

--Forward declaration
local scrollView
local F_Stage

--Variables
local themeBottom = {}
local myText = {}
local completada = {}
local navButton = {}


local function handleImageEvent ( event )
    local phase = event.phase
	local id = event.target.id
	local counter = event.target.counter
	local tipo = Datos.actividad[counter].tipo

	if ( phase == "moved" ) then
		local dy = math.abs( ( event.y - event.yStart ) )
		local dx = math.abs( ( event.x - event.xStart ) )

		-- If the touch on the button has moved more than 10 pixels,
		-- pass focus back to the scroll view so it can continue scrolling
		if ( dy > 10 or dx > 10) then
			scrollView:takeFocus( event )
			return true
		end
	elseif ( phase == "ended" ) then
		Datos.actividadTarget = id
		
		if(tipo == 1) then
			storyboard.gotoScene("sceneIntroduccion","slideLeft",200)	
		else
			storyboard.gotoScene("sceneEjercicios","slideLeft",200)		
		end
    end
	return true
end

local function handleImageEventSelect( event )
	local counter = event.target.counter

	for i=1, #themeBottom do
		themeBottom[i].isVisible = false
		myText[i].isVisible = false
		if (completada[i] ~= nil) then
			completada[i].isVisible = false
		end
	end
	
	themeBottom[counter].isVisible = true
	myText[counter].isVisible = true
	navButton[4].x = 100 + (navButton[counter].width/2) + navButton[counter].width * (counter-1)
	navButton[4].y = themeBottom[counter].y + (themeBottom[counter].height/2) + 150
	
	if (Datos.actividad[counter].completado == 1) then
		completada[counter].isVisible = true
	end
	
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	local buttonSheet
	local myGroupIMG
	myGroupIMG = display.newGroup()
  
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end

	F_Stage() --Theme title
	
	local dir = Datos.tema[idTemas].imagen  --DB images names
	local themeImage = display.newImageRect(dir,400,165) 
	themeImage.x = display.contentCenterX
	themeImage.y = 100 + (themeImage.height/2)
	myGroupIMG:insert( themeImage )
	
	local i = 1
	for j = 1 , #Datos.actividad do 

		if (Datos.actividad[j].idTheme == idTemas) then
			local dir = "assets/themeBottom.png"  --bottom Image
			
			themeBottom[i] = display.newImageRect(dir,520,450)
			themeBottom[i].x = display.contentCenterX
			themeBottom[i].y = themeImage.y + themeBottom[i].height/2 + 150
			themeBottom[i].id = Datos.actividad[j].id
			themeBottom[i].counter = i
			themeBottom[i]:addEventListener( "touch", handleImageEvent )
			themeBottom[i].isVisible = false
			myGroupIMG:insert( themeBottom[i] )
				
			local optionsTexto = 
			{
				text = Datos.actividad[j].nombre,  --Activity name
				width = themeBottom[i].width - 80,     
				height = 0, 
				font = "Montserrat-Regular",   
				fontSize = 60,
				align = "center"  
			}
			myText[i] = display.newText( optionsTexto )
			myText[i].x = themeBottom[i].x
			myText[i].y = themeBottom[i].y * .95
			myText[i].id = Datos.actividad[j].id
			myText[i].counter = i
			myText[i]:setFillColor( 1, 1, 1 )
			myText[i].isVisible = false
			myGroupIMG:insert( myText[i] )
				
			local options = {
				width = 110,
				height = 110,
				numFrames = 2,
			}
			buttonSheet = graphics.newImageSheet( "assets/navButton.png", options )

			navButton[i] = display.newImageRect(buttonSheet,1, 110, 110)
			navButton[i].x = 100 + (navButton[i].width/2) + navButton[i].width * (i-1)
			navButton[i].y = themeBottom[i].y + (themeBottom[i].height/2) + 150
			navButton[i].id = Datos.actividad[j].id
			navButton[i].counter = i
			navButton[i]:addEventListener( "touch", handleImageEventSelect )
			myGroupIMG:insert( navButton[i] )
			
			if (Datos.actividad[j].completado == 1) then  --If it's already complete add it
				local dir = "assets/completo.png" 
				completada[i] = display.newImageRect(dir,120,120) 
				completada[i].x = themeBottom[i].x
				completada[i].y = themeBottom[i].y * 1.20
				completada[i].isVisible = false
				myGroupIMG:insert( completada[i] )
			end
			i = i + 1
		end
	end
	
	--Default is "screen 1" selected (intro)
	myText[1].isVisible = true
	themeBottom[1].isVisible = true
	
	navButton[4] = display.newImageRect(buttonSheet,2, 110, 110)
	navButton[4].x = 100 + (navButton[4].width/2) 
	navButton[4].y = themeBottom[1].y + (themeBottom[1].height/2) + 150
	myGroupIMG:insert( navButton[4] )
	
	scrollView = widget.newScrollView
	{
		left = 0,
		top = 100,
		width = display.viewableContentWidth,
		height = display.viewableContentHeight,
		scrollWidth = display.viewableContentWidth,
		scrollHeight = display.viewableContentHeight,
		rightPadding = 100,
		leftPadding = 0,
		topPadding = 0,
		bottomPadding = 0,
		hideBackground = true,
		horizontalScrollingDisabled = true,
		verticalScrollingDisabled = false,
		isBounceEnabled = false
	}

	scrollView:insert( myGroupIMG )
	group:insert(scrollView)
end


F_Stage = function()
	local display_stage = display.getCurrentStage()
	
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = true	
		elseif(display_stage[i].id == "titleBarText") then
			display_stage[i].isVisible = true	
		elseif(display_stage[i].id == "salir") then
			display_stage[i].isVisible = true	
		end
		i = i + 1
	end

end



-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene
--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
------------------------------------------------------------------
-- overlayZoom.lua
-- Amplifica la imagen que se le envíe
------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local params


local function handleTouchEvent( event )
	storyboard.hideOverlay("zoomInOutFade", 100 )
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	params = event.params
	
	--Hide back and options Buttons
	local display_stage = display.getCurrentStage()
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "salir") then
			display_stage[i].isVisible = false
		end
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
		end
		i = i + 1
	end
	
	local dir = params.filename
	local ancho = params.Ancho * 3	
	local alto = params.Alto * 3
	
	if(ancho>display.contentWidth) then
		ancho = display.contentWidth
	end
	if(alto>display.contentHeight) then
		alto = display.contentHeight
	end	
	
	--Display image zoomed
	local image = display.newImageRect(dir,ancho,alto) 
	image.x = display.contentCenterX
	image.y = display.contentCenterY
	
	ancho = display.contentWidth
	alto = display.contentHeight	
	local fondo = display.newRoundedRect( display.contentCenterX, display.contentCenterY, ancho, alto, 7 )
		
	fondo:addEventListener( "touch", handleTouchEvent )
	group:insert(fondo)
	group:insert(image)
end

--When exit show back and options Buttons
function scene:exitScene( event )
	local group = self.view
	
	local display_stage = display.getCurrentStage()
	local i=1
	
	
	if(params.Ejercicios) then --Show back Button
		while display_stage[i] ~= nil do
			if(display_stage[i].id == "salir") then
				display_stage[i].isVisible = true
			end
			i = i + 1
		end
	else --Show back and options Buttons
		while display_stage[i] ~= nil do
			if(display_stage[i].id == "salir") then
				display_stage[i].isVisible = true
			end
			if(display_stage[i].id == "opciones") then
				display_stage[i].isVisible = true
			end
			i = i + 1
		end
	end
	
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

return scene
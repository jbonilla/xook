--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
---------------------------------------------------------------
-- sceneGameOver.lua
-- Animación en caso de haber contestado erróneamente 3 veces
----------------------------------------------------------------
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local Datos = require( "data" )
local htimer --Handle of the timer

local function handleTapEvent( event )
	transition.cancel( "transition" )
	storyboard.gotoScene("sceneActividades","slideLeft",1200)
	audio.stop( )
	htimer = nil
end

local function listener( event )
	if (htimer ~= nil and Datos.sound == true) then
		audio.play( Datos.gameOver,{channel = 1} )
	end
end


function scene:createScene( event )
	local group = self.view
	
	local tamenX = 408
	local tamenY = 544
	local animaX
	local animaO
	local animaO2
	local animaK
	
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end
	
	--Hide Settings Button
	local display_stage = display.getCurrentStage()
	local i=1
	while display_stage[i] ~= nil do
		if(display_stage[i].id == "opciones") then
			display_stage[i].isVisible = false
		elseif(display_stage[i].id == "salir") then
			display_stage[i].isVisible = false
		end
		i = i + 1
	end
	
	
	local function completeListener ( obj )
	----- End of animation; load sceneActividades.lua
		
		fondo = nil
		letterX = nil
		letterO = nil
		letterO2 = nil
		letterK = nil
		
		tamenX = nil
		tamenY = nil
		animaX = nil
		animaO = nil
		animaO2 = nil
		animaK = nil
		
		transition.cancel()
		storyboard.gotoScene("sceneActividades","slideLeft",1200)
	end
	
	
	local fondo = display.newImageRect( "assets/fondo.png", display.contentWidth,display.contentHeight) 
	fondo.x = display.contentCenterX
	fondo.y = display.contentCenterY
	fondo.id = 0
	fondo:addEventListener( "tap", handleTapEvent )
	group:insert( fondo )
	
	-- Create the result text
	local optionsTexto = 
	{
		text = "Game Over",     
		x = 100,
		y = 100,
		width = 550,
		height = 0,
		font = "Montserrat-Regular",   
		fontSize = 85,
		align = "center"  
	}
	local resultText = display.newText( optionsTexto )
	resultText.anchorX = 0
	resultText.anchorY = 0
	resultText:setFillColor(1, 1, 1)
	group:insert( resultText )
	
	local letterX = display.newImageRect( "assets/X.png", tamenX,tamenY) 
	letterX.x = 230
	letterX.y = 620
	letterX.id = 1
	letterX:addEventListener( "tap", handleTapEvent )
	group:insert( letterX )
	
	local letterO = display.newImageRect( "assets/O.png", tamenX,tamenY) 
	letterO.x = 520
	letterO.y = 510
	letterO.id = 2
	letterO:addEventListener( "tap", handleTapEvent )
	group:insert( letterO )
	
	local letterO2 = display.newImageRect( "assets/O.png", tamenX,tamenY) 
	letterO2.x = 310
	letterO2.y = 1000
	letterO2.id = 3
	letterO2:addEventListener( "tap", handleTapEvent )
	group:insert( letterO2 )
	
	local letterK = display.newImageRect( "assets/K.png", tamenX,tamenY)
	letterK.x = 600
	letterK.y = 890
	letterK.id = 4
	letterK:addEventListener( "tap", handleTapEvent )
	group:insert( letterK )
	
	-----------------Start animating--------------------
	
	htimer = timer.performWithDelay( 950, listener )
	animaX = transition.to(letterX,{
	time = 1000,
	alpha = 0,
	y = display.contentHeight,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 1900, listener ) })
	

	animaO = transition.to(letterO,{
	time = 1000,
	delay = 1000,
	alpha = 0,
	y = display.contentHeight,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 2850, listener )})	
	
	
	animaO2 = transition.to(letterO2,{
	time = 1000,
	delay = 2000,
	alpha = 0,
	y = display.contentHeight,
	transition = easing.linear,
	tag="transition",
	onComplete = timer.performWithDelay( 3800, listener )})
	

	animaK = transition.to(letterK,{
	time = 1000,
	delay = 3000,
	alpha = 0,
	y = display.contentHeight,
	transition = easing.linear,
	tag="transition",
	onComplete = completeListener})

end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene
--[[ Copyright 2016 Julio Bonilla

	This file is part of XOOK.

    XOOK is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XOOK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XOOK.  If not, see <http://www.gnu.org/licenses/>.
--]]
----------------------------------------------------------------------
-- scenecero.lua
-- Carga los elementos que serán constantes a lo largo de toda la app;
-- fondo y barra de título.
----------------------------------------------------------------------
local Datos = require( "data" )
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
----------------------------------------------------------------------
local handleButtonEvent
local handleButtonAlert
local show


function scene:createScene( event )
	local group = self.view
	
	if(not(storyboard.getPrevious() == nil)) then
		storyboard.removeScene(storyboard.getPrevious())
	end	
			
	-- Load bottom image 
	local fondo = display.newImageRect( "assets/fondo.png", display.contentWidth,display.contentHeight) 
	fondo.x = display.contentCenterX
	fondo.y = display.contentCenterY
	fondo.id = "fondo"
	
	-- Load image for title bar bottom
	local titleBarBottom = display.newImageRect( "assets/titleBarBottom.png", display.contentWidth,110) 
	titleBarBottom.x = fondo.x
	titleBarBottom.y = titleBarBottom.height/2
	titleBarBottom.id = "titleBarBottom"
	
	-- Load title bar text (XOOK)
	local titleBarText = display.newImageRect( "assets/XOOK.png", 380,110) 
	titleBarText.x = display.contentCenterX
	titleBarText.y = titleBarBottom.y
	titleBarText.id = "titleBarText"
	
	-- Load image for subTitle bar bottom
	local subTitleBarBottom = display.newImageRect( "assets/subTitleBarBottom.png", display.contentWidth,55) 
	subTitleBarBottom.x = display.contentCenterX
	subTitleBarBottom.y = titleBarBottom.height + 22.5 -- (subTitleBarBottom.height/2)
	subTitleBarBottom.id = "subTitleBarBottom"
	
	--image sheet options and declaration
	local options = {
	   width = 55,
	   height = 55,
	   numFrames = 2,
	}
	local buttonSheet = graphics.newImageSheet( "assets/backSheet.png", options )

	-- Create backButton for the title bar
	local backButton = widget.newButton
	{
	   id = "salir",
	   sheet = buttonSheet,  --reference to the image sheet
	   defaultFrame = 1,     --number of the "default" frame
	   overFrame = 2,        --number of the "over" frame
	   onRelease  = handleButtonEvent
	}
	backButton.x = 100
	backButton.y = subTitleBarBottom.y
	
	--image sheet declaration
	local buttonSheet = graphics.newImageSheet( "assets/optionSheet.png", options )

	-- Create settingsButton for the title bar
	local settingsButton = widget.newButton
	{
	   id = "opciones",
	   sheet = buttonSheet,  
	   defaultFrame = 1,     
	   overFrame = 2,        
	   onRelease  = show
	}
	settingsButton.x = 675
	settingsButton.y = subTitleBarBottom.y

	--Create Title Bar Group
	local display_stage = display.getCurrentStage() 
	display_stage:insert( 1,fondo )
	display_stage:insert( 2,titleBarBottom )
	display_stage:insert( 3,titleBarText )
	display_stage:insert( 4,subTitleBarBottom )
	display_stage:insert( backButton )
	display_stage:insert( settingsButton )

	-- load sceneTemas.lua
	storyboard.gotoScene("sceneTemas","slideLeft",1)
end

-- Go to previous scene
handleButtonEvent = function( event )
	local sceneName = storyboard.getCurrentSceneName()
	
	if(sceneName==Datos.scenes[1])then
		native.requestExit()
	elseif(storyboard.isOverlay) then
			storyboard.hideOverlay()
	elseif(sceneName == Datos.scenes[2] or sceneName == Datos.scenes[7]) then
		storyboard.gotoScene(Datos.scenes[1],"slideRight",200)
	elseif(sceneName  == Datos.scenes[4]) then
		native.showAlert("Salir","El progreso se perderá, aún así desea salir", {"Si", "No"},handleButtonAlert)
	else
		storyboard.gotoScene(Datos.scenes[2],"slideRight",200)
	end
	
	return true
end

-- User: are you sure you want to exit
handleButtonAlert = function  ( event )
	if "clicked" == event.action then
        local i = event.index
        if i == 1 then --If yes
			storyboard.gotoScene(Datos.scenes[2],"slideRight",200)
        end
    end
end


--Show options
show = function (event)
	
	if Datos.showing then
		storyboard.hideOverlay("fade",0)
	else
		local options =
		{
			effect = "flipFadeOutIn",
			time = 0,
		}
		storyboard.showOverlay( "overlayOpciones", options) 
	end
	
	Datos.showing = not Datos.showing
end


-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )


return scene